/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package gui;

import java.awt.*;
import javax.swing.*;

import swiat.*;

/**
 *
 * @author Wojtek
 */



public class View {
    
    private JButton nowaTura;
    public JTextArea textArea;
    public JScrollPane scrollPane;
    private JFrame window;
    public JButton zapisz;
    private JButton wczytaj;
    
    private Swiat swiat;
    private Grid grid;
    
    public View() {
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
       } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {}
        window = new JFrame();
        textArea = new JTextArea();
        nowaTura = new JButton();
        zapisz = new JButton();
        wczytaj = new JButton();
        grid = new Grid();
        
        scrollPane = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        nowaTura.setBounds(10, 725, 100, 30);
        nowaTura.setText("Nowa tura");
        nowaTura.setVisible(true);
        textArea.setColumns(5);
        textArea.setBackground(Color.WHITE);
        textArea.setEditable(false);
        textArea.setAutoscrolls(true);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        Font font = textArea.getFont();
        
        textArea.setFont( font.deriveFont(4, font.getSize() + 3.0f) );
        
        textArea.setText("Witam w wirtualnym swiecie!\n "
                + "wczytuje poczatkowe organizmy ...\n");
        
        scrollPane.setBounds(10, 520, 500, 200);
        
        window.setSize(525, 800);
        window.setTitle("Wirtualny Swiat v2.0");
        window.setResizable(false);
        //grid.addMouseListener(new PopClickListener());
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation(dim.width/2-window.getSize().width/2, dim.height/2-window.getSize().height/2);
        
        window.getContentPane().add(scrollPane);
  
        window.add(nowaTura);
  
        wczytaj.setBounds(305, 725, 100, 30);
        wczytaj.setText("Wczytaj");
        window.add(wczytaj);
        
        
        zapisz.setBounds(410, 725, 100, 30);
        zapisz.setText("Zapisz");
        zapisz.setVisible(true);
        window.add(zapisz);
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.add(grid);
 
        scrollPane.setVisible(true);
        textArea.setVisible(true);
        wczytaj.setVisible(true);
        window.setVisible(true);
        
        reload();   
    }
     public void reload() {
         SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                window.repaint();   
            }
        });
     }
     
     public void setText(String text) {
         textArea.setText(text);
     }
     
     public void addNewCell(int x, int y, Color c) {
         grid.dodajNowyColoredPoint(x, y, c);
     }
     
     public JButton getButton() {
         return nowaTura;
     } 
     
     public JButton getSaveButton() {
         return zapisz;
     } 
     
     public JButton getLoadButton() {
         return wczytaj;
     } 
     
     public Grid getGrid() {
         return grid;
     }
}


