/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.awt.Color;
import java.awt.event.*;

import swiat.Swiat;

/**
 *
 * @author Wojtek
 */

public class MenuListener implements MouseListener {
    private Swiat swiat;
    private int _x;
    private int _y;
    
    public MenuListener(Swiat swiat) {
        this.swiat = swiat;
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isPopupTrigger()) {
            doPop(e);
        }
    }
    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            doPop(e);
        }
    }
    private void doPop(MouseEvent e) {
        PopUpMenu menu = new PopUpMenu(swiat, e.getX() / 25, e.getY() / 25);
        menu.show(e.getComponent(), e.getX(), e.getY());
        _x = e.getX() / 25;
        _y = e.getY() / 25;
    }
}
