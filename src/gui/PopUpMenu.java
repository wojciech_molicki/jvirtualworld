/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import swiat.*;

/**
 *
 * @author Wojtek
 */
public class PopUpMenu extends JPopupMenu {
    private Swiat swiat;
    
    JMenuItem owca;
    JMenuItem wilk;
    JMenuItem zolw;
    JMenuItem trawa;
    JMenuItem leniwiec;
    JMenuItem guziec;
    JMenuItem guarana;
    JMenuItem ciern;
    
    private int _x;
    private int _y;
    
    public PopUpMenu(Swiat swiat, int x, int y) {
        this.swiat = swiat;
        _x = x;
        _y = y;
        
        owca = new JMenuItem("Owca", new colorIcon(new Owca(null, 0,0).getColor()));
        wilk = new JMenuItem("Wilk", new colorIcon(new Wilk(null, 0,0).getColor()));
        guarana = new JMenuItem("Guarana", new colorIcon(new Guarana(null, 0,0).getColor()));
        guziec = new JMenuItem("Guziec", new colorIcon(new Guziec(null, 0,0).getColor()));
        leniwiec = new JMenuItem("Leniwiec", new colorIcon(new Leniwiec(null, 0,0).getColor()));
        trawa = new JMenuItem("Trawa", new colorIcon(new Trawa(null, 0,0).getColor()));
        ciern = new JMenuItem("Ciern", new colorIcon(new Ciern(null, 0,0).getColor()));
        zolw = new JMenuItem("Zolw", new colorIcon(new Zolw(null, 0,0).getColor()));
        
        this.add(owca);
        this.add(wilk);
        this.add(guziec);
        this.add(leniwiec);
        this.add(zolw);
        this.addSeparator();
        this.add(ciern);
        this.add(trawa);
        this.add(guarana);
        
        owca.addActionListener(new MenuItemListener(swiat, _x, _y));
        wilk.addActionListener(new MenuItemListener(swiat, _x, _y));
        guziec.addActionListener(new MenuItemListener(swiat, _x, _y));
        leniwiec.addActionListener(new MenuItemListener(swiat, _x, _y));
        zolw.addActionListener(new MenuItemListener(swiat, _x, _y));
        ciern.addActionListener(new MenuItemListener(swiat, _x, _y));
        trawa.addActionListener(new MenuItemListener(swiat, _x, _y));
        guarana.addActionListener(new MenuItemListener(swiat, _x, _y));
    }
}

class MenuItemListener implements ActionListener {
    
    private Swiat swiat;
    private int _x;
    private int _y;
    
    public MenuItemListener(Swiat swiat, int x, int y) {
        this.swiat = swiat;
        _x = x;
        _y = y;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch(command) {
            case "Owca":
                swiat.dodajOrganizm(_x, _y, 'O');
                swiat.rysujSwiat();
                break;
            case "Wilk":
                swiat.dodajOrganizm(_x, _y, 'W');
                swiat.rysujSwiat();
                break;
            case "Guarana":
                swiat.dodajOrganizm(_x, _y, 'G');
                swiat.rysujSwiat();
                break;
            case "Trawa":
                swiat.dodajOrganizm(_x, _y, 'T');
                swiat.rysujSwiat();
                break;
            case "Guziec":
                swiat.dodajOrganizm(_x, _y, 'U');
                swiat.rysujSwiat();
                break;
            case "Leniwiec":
                swiat.dodajOrganizm(_x, _y, 'L');
                swiat.rysujSwiat();
                break;
            case "Zolw":
                swiat.dodajOrganizm(_x, _y, 'Z');
                swiat.rysujSwiat();
                break;
            case "Ciern":
                swiat.dodajOrganizm(_x, _y, 'C');
                swiat.rysujSwiat();
                break;
            
            default:
                System.out.printf("NIE");
                break;
        }
    }
}

class colorIcon implements Icon {
    private static final int HEIGHT = 15;
    private static final int WIDTH = 15;
    private final Color color;
    
    public colorIcon(Color c) {
        color = new Color(c.getRGB());
    }
    
    @Override
    public int getIconHeight()
    {
        return HEIGHT;
    }
    
    @Override
    public int getIconWidth()
    {
        return WIDTH;
    }
    
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y)
    {
        g.setColor(color);
        g.fillRect(x, y, WIDTH, HEIGHT);
    }
}
