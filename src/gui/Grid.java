/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Wojtek
 */
public class Grid extends JPanel {

        final static private int ROWS = 20;
        final static private int COLS = 20;
        final static private int BOX_SIZE = 25;
        final static private int WINDOW_SPACING = 10;
        
        private LinkedList<coloredPoint> fillCells;

        public Grid() {
            fillCells = new LinkedList<>();
        }
        
        public LinkedList<coloredPoint> getFillCellsList() {
            return fillCells;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            g.setColor(Color.WHITE);
            g.fillRect(WINDOW_SPACING, WINDOW_SPACING, BOX_SIZE*ROWS, BOX_SIZE*COLS);
            
            for (coloredPoint fillCell : fillCells) {
                //System.out.printf("rysuje %d %d\n", fillCell.p.x, fillCell.p.y);
                int cellX = WINDOW_SPACING + (fillCell.p.x * BOX_SIZE);
                int cellY = WINDOW_SPACING + (fillCell.p.y * BOX_SIZE);
                g.setColor(fillCell.c);
                g.fillRect(cellX, cellY, BOX_SIZE, BOX_SIZE);
            }
            
            g.setColor(Color.BLACK);
            g.drawRect(WINDOW_SPACING, WINDOW_SPACING, ROWS*BOX_SIZE, COLS*BOX_SIZE);
        }

        public void dodajNowyColoredPoint(int x, int y, Color c) {
            fillCells.add(new coloredPoint(x, y, c));
        }
        
        public void odswiez() {
            System.out.printf("%d\n", fillCells.size());
            SwingUtilities.invokeLater(new Runnable() {
            @Override
                public void run() { repaint(); } 
            });
        }

        public void setColor(int x, int y, Color c) {
            for (coloredPoint fillCell : fillCells) {
                if (fillCell.p.x == x && fillCell.p.y == y) {
                    fillCell.c = c;
                }
            }
            repaint();
        }
        
}