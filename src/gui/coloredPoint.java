/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Wojtek
 */
public class coloredPoint {
    Point p;
    Color c;

    public coloredPoint(int x, int y, Color color) {
        p = new Point(x, y);
        c = new Color(color.getRGB());
    }
}