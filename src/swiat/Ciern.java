/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Ciern extends Roslina {
    public Ciern(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(102, 51, 0));
        setPRozmnazania(0.2);
        setName("Ciern");
        setTyp('C');
        setSila(2);
    }
}
