/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Leniwiec extends Zwierze {
    public Leniwiec(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(0, 102, 204));
        setInicjatywa(1);
        setTyp('L');
        setName("Leniwiec");
    }
    
    @Override
    public void akcja() {
        if (this.getJestZywy() == false) {
            return;
        }
        if (_ruszalSieOstatnio == false) {
            super.akcja();
            _ruszalSieOstatnio = true;
        } 
        else {
            _ruszalSieOstatnio = false;
            getSwiat().komunikat("Leniwiec postanowil tym razem sie nie ruszac.");
        }
    }
    
    private boolean _ruszalSieOstatnio = false;
}