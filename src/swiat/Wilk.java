/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Wilk extends Zwierze {
    public Wilk(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(160,160,160));
        setInicjatywa(5);
        setSila(9);
        setTyp('W');
        setName("Wilk");
    }
}
