/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.io.*;
import static java.lang.Integer.parseInt;
import java.util.*;

/**
 *
 * @author Wojtek
 */
public class Serializator  {
    
    private Swiat swiat;
    
    public Serializator(Swiat swiat)  {
        this.swiat = swiat;
    }
    
    public void zapisz() {
        try {
           PrintWriter out = new PrintWriter("organizmy.txt");
           LinkedList<Organizm> o = swiat.getOrganizmy();
           for (Organizm i : o) {
               out.println(i.getWiek());
               out.println(i.getSila());
               out.println(i.getPole());
               out.println(i.getTyp());            
           }
           out.close();
      
        } catch (IOException e) {
            e.printStackTrace();
        }     
    }
    
    public void wczytaj() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("organizmy.txt"));
            String line;
            swiat.getOrganizmy().clear();
            while ((line = br.readLine()) != null) {
              int wiek = parseInt(line);
              int sila = parseInt(br.readLine());
              int pole = parseInt(br.readLine());
              char typ = br.readLine().charAt(0);
              
              try {
                Organizm w = FabrykaOrganizmow.utworzOrganizm(swiat, typ, pole % 20, pole / 20);
                w.setWiek(wiek);
                w.setSila(sila);
                swiat.getOrganizmy().add(w);
              } catch (BadOrganizmException e) {
                  System.out.printf("%s", e.wypiszPowod());
              }
              
              
            }  
            swiat.rysujSwiat();
            br.close();
        } catch (IOException e) {
            swiat.getOrganizmy().clear();
            e.printStackTrace();
        }
    }


    


}
