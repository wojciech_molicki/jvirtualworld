/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import gui.MenuListener;
import gui.View;
import gui.coloredPoint;

import java.awt.event.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author Wojtek
 */

public class Swiat implements ActionListener {

    private View view;
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == view.getButton()) {
            wykonajTure();
            view.reload();
        }
        if (e.getSource() == view.getSaveButton()) {
            System.out.printf("save\n");
            Serializator s = new Serializator(this);
            s.zapisz();  
        }
        if (e.getSource() == view.getLoadButton()) {
            System.out.printf("load\n");
            Serializator s = new Serializator(this);
            s.wczytaj();
        }
    }
    
    public Swiat(View view)  {
        this.view = view;
        _turaNumer = 0;
        listaInicjatyw = new LinkedList<LinkedList<Organizm>>();
        for (int i = 0; i < maxInicjatywa; i++) {
            listaInicjatyw.add(new LinkedList<Organizm>());
        }
        listaKomunikatow = new LinkedList<String>();
        organizmy = new LinkedList<Organizm>();
        _randomGenerator = new Random();
        
        view.getButton().addActionListener(this);
        view.getLoadButton().addActionListener(this);
        view.getSaveButton().addActionListener(this);
        
        view.getGrid().addMouseListener(new MenuListener(this));
        
        rozmiescLosoweOrganizmy();
        rysujSwiat();     
    }
    
    public void wykonajTure() {
        przypiszInicjatywy();
        akcjaOrganizmow();
        usunNiezywe();
        wyswietlKomunikaty();
        posunCzas();
        rysujSwiat();
    }

    public void komunikat(String wiadomosc) {
        listaKomunikatow.add(wiadomosc);
    }
    
    public View getView() {
        return view;
    }
    
    public Random getRandomGenerator() {
        return _randomGenerator;
    }

    public Organizm czyKolizja(int x, int y) {
        for (Organizm organizm : organizmy) {
           if (organizm == null) {
               continue;
           }
           if (organizm.getJestZywy() == false) {
               continue;
           }
           if (x == organizm.getX() && y == organizm.getY())
           {
               return organizm;
           }
        } 
        return null;
    }
    
    private void cleanOrganizmy() {
        LinkedList<Organizm> nOrganizmy = new LinkedList<Organizm>();
        while(organizmy.isEmpty() == false) {
            if (organizmy.getFirst() != null) {
                nOrganizmy.add(organizmy.remove());
            }
        }
        organizmy = nOrganizmy;
    }
    
    public void dodajOrganizm(int x, int y, char typ) {
        try {
            organizmy.add(FabrykaOrganizmow.utworzOrganizm(this, typ, x, y));
            view.reload(); 
        } catch (BadOrganizmException e) {
            System.out.printf("%s\n", e.wypiszPowod());
        }
    }
    
    public void dodajOrganizm(int x, int y, int typ) {
        try {
            organizmy.add(FabrykaOrganizmow.utworzOrganizm(this, typ, x, y));
            view.reload(); 
        } catch (BadOrganizmException e) {
            System.out.printf("%s\n", e.wypiszPowod());
        }
    }

    private static final int startowaIloscOrganizmow = 20;
    private static final int maxInicjatywa = 8;
    private static final int maxIloscOrganizmow = 400;
    
    private Random _randomGenerator;

    int _turaNumer;
    
    private LinkedList<String> listaKomunikatow;
    private LinkedList<Organizm> organizmy;
    private LinkedList <LinkedList<Organizm>> listaInicjatyw;

    public LinkedList<Organizm> getOrganizmy() {
        return organizmy;
    }
    
    public void rysujSwiat() {
        LinkedList<coloredPoint> fillCells = view.getGrid().getFillCellsList();
        fillCells.clear();
        
        for (Organizm organizm : organizmy) {
            //System.out.printf("%s", organizm.getName());
            if (organizm != null && organizm.getJestZywy())
                fillCells.add(organizm.rysuj());
        }
        view.reload();
    }
    
    private void rozmiescLosoweOrganizmy() {
        for (int i = 0; i < startowaIloscOrganizmow; i++) {
            int randomPole = _randomGenerator.nextInt(maxIloscOrganizmow);
            int randomOrganizm = _randomGenerator.nextInt(8);
            int r_x = randomPole % 20;
            int r_y = randomPole / 20;
            if (this.czyKolizja(r_x, r_y) == null) 
                dodajOrganizm(r_x, r_y, randomOrganizm);
            else {
                i--;
            }
        }
    }
    
    private void wyswietlKomunikaty() {
        String komunikat = "";
        while (listaKomunikatow.isEmpty() == false) {
            komunikat = komunikat + listaKomunikatow.remove() + "\n";
        }
        view.setText(komunikat);
    }
    
    private void akcjaOrganizmow() {
        Comparator<Organizm> comp = new Comparator<Organizm>() {
            @Override
            public int compare (Organizm o1, Organizm o2) {
                return o1.getWiek() - o2.getWiek();
            }
        };
        for (LinkedList<Organizm> listaOrganizmow : listaInicjatyw) {
            Collections.sort(listaOrganizmow, comp);
            while(listaOrganizmow.isEmpty() == false) {
                if (listaOrganizmow.get(0).getJestZywy())
                    listaOrganizmow.get(0).akcja();
                listaOrganizmow.remove(0);
            }
        }
    }
    
    private void przypiszInicjatywy() {
        for (Organizm organizm : organizmy) {
           if (organizm != null) {
                listaInicjatyw.get(organizm.getInicjatywa()).add(organizm);
           }
        } 
    }
    
    private void posunCzas() {
        for (Organizm organizm : organizmy) {
            if (organizm != null) {
                organizm.setRozmnazalSie(false);
                organizm.postarz();
            }
        }
        _turaNumer++;
    }
    
    private void usunNiezywe() {
       //organizmy.removeAll(Collections.singleton(null));
       //cleanOrganizmy();
       Comparator<Organizm> comp = new Comparator<Organizm>() {
            @Override
            public int compare (Organizm o1, Organizm o2) {
                int o1_int = o1.getJestZywy() ? 1 : 0;
                int o2_int = o2.getJestZywy() ? 1 : 0;
                return o1_int - o2_int;
            }
        };
       Collections.sort(organizmy, comp);
       int usunieto = 0;
       System.out.printf("%b\n", organizmy.getFirst().getJestZywy());
       while(organizmy.getFirst().getJestZywy() == false) {
           organizmy.removeFirst();
           usunieto++;
       }
       System.out.printf("%d\n", usunieto);
    }
}

