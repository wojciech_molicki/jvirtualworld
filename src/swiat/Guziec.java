/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Guziec extends Zwierze {
    public Guziec(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(255, 51, 51));
        setInicjatywa(6);
        setSila(3);
        setTyp('U');
        setName("Guziec");
    }
    
    @Override
    public int getSila() {
        return super.getSila() + _bonusSila;
    }
    
    @Override
    public void kolizja(Organizm o) {
        super.kolizja(o);
        if (this.getJestZywy()) {
            this._bonusSila++;
            getSwiat().komunikat("Guziec po pokonaniu " + o.getName() + " staje sie silniejszy!");
        }
    }
    
    private int _bonusSila;
}