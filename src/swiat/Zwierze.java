/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

/**
 *
 * @author Wojtek
 */
public class Zwierze extends Organizm {
    public Zwierze(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setRozmnazalSie(false);
    }
    
    @Override
    public void akcja() {
        if (getJestZywy() == false) {
            return;
        }
        
        int pole = wybierzLosowePole();
        int x = pole % 20;
        int y = pole / 20;
        
        Organizm kolidujacy = getSwiat().czyKolizja(x, y);
        if (kolidujacy != null) {
            kolizja(kolidujacy);
            kolidujacy.kolizja(this);
        }
        else {
            setXY(x, y);
        }
    }
    
    @Override
    public void kolizja(Organizm o) {
        if (getName().compareTo(o.getName()) == 0) {
            //rozmnazanie
            
            int pole = wybierzLosoweWolnePole();
            int x = pole % 20;
            int y = pole / 20;
            
            //&& 
            if (this.getRozmnazalSie() == false && pole != -1) {
                getSwiat().dodajOrganizm(x, y, getTyp());
                getSwiat().komunikat("Rozmnazanie sie organizmow typu" + getName());
                this.setRozmnazalSie(true);
            }
        }
        else {
            //walka 
            if (getSila() > o.getSila())
                o.setJestZywy(false);
            else {
                setJestZywy(false);
            }
            if (getJestZywy()) {
                getSwiat().komunikat("[ATAK]: " + getName() + " => [OBRONA]: " +
				o.getName() + " | ginie: " + o.getName());
                setXY(o.getX(), o.getY());
            }
            else {
                getSwiat().komunikat("[ATAK]: " + getName() + " => [OBRONA]: " +
				o.getName() + " | ginie: " + getName());
            }
        }
    }
}
