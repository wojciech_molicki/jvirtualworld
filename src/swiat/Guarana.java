/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Guarana extends Roslina {
    public Guarana(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(128, 255, 0));
        setName("Guarana");
        setTyp('G');
    }
    
    @Override
    public void kolizja(Organizm o) {
        o.setSila(o.getSila() + 3);
	getSwiat().komunikat("Guarana zwieksza sile " + o.getName());
    }
}
