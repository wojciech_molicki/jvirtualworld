/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Trawa extends Roslina {
    public Trawa(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(0,153,0));
        setTyp('T');
        setName("Trawa");
    }
}
