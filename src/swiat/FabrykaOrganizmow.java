/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

/**
 *
 * @author Wojtek
 */

public class FabrykaOrganizmow {
    public static Organizm utworzOrganizm(Swiat swiat, char typ, int x, int y) 
            throws BadOrganizmException {
        switch(typ) {
            case 'C':
                return new Ciern(swiat, x, y);
                
            case 'G':
                return new Guarana(swiat, x, y);
                
            case 'U':
                return new Guziec(swiat, x, y);
                
            case 'L':
                return new Leniwiec(swiat, x, y);
                
            case 'O':
                return new Owca(swiat, x, y);
                
            case 'T':
                return new Trawa(swiat, x, y);
                
            case 'W':
                return new Wilk(swiat, x, y);
                
            case 'Z':
                return new Zolw(swiat, x, y);
                
            default:
                return null;
        }
    }
    
    public static Organizm utworzOrganizm(Swiat swiat, int typ, int x, int y)
        throws BadOrganizmException {
        switch(typ) {
            case 0:
                return new Ciern(swiat, x, y);
                
            case 1:
                return new Guarana(swiat, x, y);
                
            case 2:
                return new Guziec(swiat, x, y);
                
            case 3:
                return new Leniwiec(swiat, x, y);
                
            case 4:
                return new Owca(swiat, x, y);
                
            case 5:
                return new Trawa(swiat, x, y);
                
            case 6:
                return new Wilk(swiat, x, y);
                
            case 7:
                return new Zolw(swiat, x, y);
                
            default:
                return null;
                
        }
    }
}
