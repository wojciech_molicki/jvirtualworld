/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

/**
 *
 * @author Wojtek
 */
public class Roslina extends Organizm {
    
    public double getPRozmnazania() {
        return _pRozmnazania;
    }
    
    public void setPRozmnazania(double pRozmnazania) {
        _pRozmnazania = pRozmnazania;
    }
  
    public Roslina(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setSila(0);
        setInicjatywa(0);
        setPRozmnazania(0.1);
    }
    
    @Override
    public void akcja() {
        if (getJestZywy() == false) {
            return;
        }
        int losowa = getSwiat().getRandomGenerator().nextInt(100);
        //System.out.printf("%d %f\n", losowa, _pRozmnazania * 100);
        if (losowa < _pRozmnazania * 100) {
            int pole = wybierzLosoweWolnePole();
            if (pole != -1) {
                getSwiat().dodajOrganizm(pole % 20, pole / 20, getTyp());
                getSwiat().komunikat("Rozmnazanie organizmow typu: " + getName());
            }
        }
    }
    
    @Override
    public void kolizja(Organizm o) {
        
    }
    
    private double _pRozmnazania = 0.2;
}
