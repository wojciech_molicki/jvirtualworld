/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;

/**
 *
 * @author Wojtek
 */
public class Owca extends Zwierze {
    public Owca(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(new Color(255, 200, 220));
        setInicjatywa(4);
        setSila(4);
        setTyp('O');
        setName("Owca");
    }
}
