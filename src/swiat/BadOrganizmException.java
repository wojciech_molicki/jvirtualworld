/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wojtek
 */

package swiat;

public class BadOrganizmException extends Exception {
    public BadOrganizmException(String message) {
        super(message);
    }
    
    public String wypiszPowod() {
        return "Wyjatek spowodowalo: " + super.getMessage();
    }
}
