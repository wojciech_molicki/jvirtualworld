/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author Wojtek
 */
public class Zolw extends Zwierze {
    public Zolw(Swiat swiat, int x, int y) {
        super(swiat, x, y);
        setColor(Color.YELLOW);
        setInicjatywa(1);
        setSila(2);
        setTyp('Z');
        setName("Zolw");
    }
    
    @Override
    public void akcja() {
        if (getJestZywy() == false)
            return;
        int p = getSwiat().getRandomGenerator().nextInt(100);
        if (p > 75) {
            super.akcja();
            getSwiat().komunikat("Zolw postanowil sie ruszyc");
        } 
    }
    
    
}
