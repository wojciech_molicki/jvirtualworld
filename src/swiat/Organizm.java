/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swiat;

import gui.coloredPoint;
import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Wojtek
 */
public class Organizm {
    
    public Organizm(Swiat swiat, int x, int y) {
        this.swiat = swiat;
        _jestZywy = true;
        _color = Color.BLACK;
        _name = "noname";
        _wiek = 0;
        setInicjatywa(0);
        setXY(x,y);
    }
    
    public coloredPoint rysuj() {
        return new coloredPoint(_x, _y, _color);
    }
    
    public void akcja() {
        
    }
    
    public void kolizja(Organizm o) {
        
    }
    
    public void sasiedniePola(int tab[][]) {
       tab[0][0] = _x+1; tab[0][1] = _y;
	tab[1][0] = _x-1; tab[1][1] = _y;
	tab[2][0] = _x; tab[2][1] = _y+1;
	tab[3][0] = _x; tab[3][1] = _y-1;
	tab[4][0] = _x+1; tab[4][1] = _y+1;
	tab[5][0] = _x+1; tab[5][1] = _y-1;
	tab[6][0] = _x-1; tab[6][1] = _y+1;
	tab[7][0] = _x-1; tab[7][1] = _y-1;
    }
    
    public void postarz() {
        _wiek++;
    }
    
    public boolean getRozmnazalSie() {
        return _rozmnazalSie;
    }
    
    public void setRozmnazalSie(boolean rozmnazalSie) {
        _rozmnazalSie = rozmnazalSie;
    }
    
    String getName() {
        return _name;
    }
    
    void setName(String name) {
        _name = name;
    }
    
    int getX() {
        return _x;
    }
    
    int getY() {
        return _y;
    }
    
    int getPole() {
        return _x + _y * 20;
    }
    
    void setXY(int x, int y) {
        _x = x;
        _y = y;
    }
    
    int getSila() {
        return _sila;
    }
    
    public void setSila(int sila) {
        _sila = sila;
    }
    
    public Color getColor() {
        return _color;
    }
    
    void setColor(Color color) {
        _color = color;
    }
    
    char getTyp() {
        return _typ;
    }
    
    void setTyp(char typ) {
        _typ = typ;
    }
    
    int getWiek() {
        return _wiek;
    }
    
    public void setWiek(int wiek) {
        _wiek = wiek;
    }
    
    int getInicjatywa() {
        return _inicjatywa;
    }
    
    void setInicjatywa(int inicjatywa) {
        _inicjatywa = inicjatywa;
    }
    
    boolean getJestZywy() {
        return _jestZywy;
    }
    
    void setJestZywy(boolean zywy) {
        _jestZywy = zywy;
    }
    
    Swiat getSwiat() {
        return swiat;
    }

    int wybierzLosowePole() {
        int sasiednie[][] = new int[8][2];
        sasiedniePola(sasiednie);
        
        boolean legalne = false;
        int pole = 0;
        while (legalne == false) {
            pole = swiat.getRandomGenerator().nextInt(8);
            if (isLegalMove(sasiednie[pole][0], sasiednie[pole][1])) {
                legalne = true;
            }
        }
        return sasiednie[pole][1] * 20 + sasiednie[pole][0];      
    }
    
    int wybierzLosoweWolnePole() {
        int sasiednie[][] = new int[8][2];
        sasiedniePola(sasiednie);
        
        ArrayList<Integer> wolne_pola = new ArrayList<>();
        
        for (int i = 0; i < 8; i++) {
            if (isLegalMove(sasiednie[i][0], sasiednie[i][1])) {
                if (getSwiat().czyKolizja(sasiednie[i][0], sasiednie[i][1]) == null) {
                    wolne_pola.add(sasiednie[i][1] * 20 + sasiednie[i][0]);
                }
            }
        }
        
        int ilosc = wolne_pola.size();
        System.out.printf("wolne_pola size=%d\n", ilosc);
        int wynik = -1;
        
        if (ilosc > 0) {
            wynik = wolne_pola.get(swiat.getRandomGenerator().nextInt(ilosc));
        }
        
        return wynik;
    }
    
    boolean isLegalMove(int x, int y) {
        return !(x >= 20 || x < 0 || y >= 20 || y < 0);
    }

    private int _wiek;
    private boolean _jestZywy;
    private int _x;
    private int _y;
    
    private boolean _rozmnazalSie;
    
    private Swiat swiat;
    private Color _color;
    private char _typ;
    private int _sila;
    private int _inicjatywa;
    private String _name;
    
    
    
}


